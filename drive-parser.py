"""
This is a short script to merge all the PDF's located in a specified column of a CSV file
Run this code with 'python2 drive-parser.py '<inputFile>' '<targetColumn>' '<outputFile>''
	Note that you can also use 'py -2 drive-parser.py' if your path is messed up like mine :(
	@param inputFile = the location of the .csv file that contains the files to be downloaded
	@param targetColumn = the column # (1-indexed) that contains the documents to be downloaded
	@param outputFile = the location of the pdf that you want to receive that merges all the pdf's in the inputFile
	
Dependencies:
PyDrive: (can be gotten by calling 'pip install PyDrive' in Windows, see the website: http://pythonhosted.org/PyDrive/index.html)
	Note that to you PyDrive, you'll need to follow the setup instructions on the main page and Quick Start pages of their website
		The most important of these is how you'll need to register the account in question for use with the Google Drive API, and how
		you'll need to download the client_secret<id>.json file from the Google Authentication page.

PyPDF2: (can be gotten by calling 'pip install PyPdf2' in Windows, see the website: https://pythonhosted.org/PyPDF2/index.html)
"""

#For downloading documents from Google Drive
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
#For merging pdfs
from PyPDF2 import PdfFileMerger
#For parsing the files that need to be downloaded
import csv
import re
#Intermediate package used when merging files
import os
import sys

"""
TODO: Change the regex used to find the ID to one that works for all Google Drive addresses
"""
def parseFileNames(fileName, targetColumn):
	filesToLoad = [] #list of id's to load

	with open(fileName, 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='|')
		for row in reader:
			matchObj = re.match('.*id=(.*)', row[targetColumn - 1]) #targetColumn = 1-indexed
			if matchObj == None:
				continue
			filesToLoad.append(matchObj.group(1))
	return filesToLoad
	
def mergeFiles(filesToLoad, outputFileName, drive, maxFilesPerBatch=10):
	N = len(filesToLoad) // (maxFilesPerBatch+1) + 1 #N = the number of times we need to merge files to complete the task
	merger = PdfFileMerger()
	for i in range(N):
		for j in range(maxFilesPerBatch):
			if N*i+j >= len(filesToLoad): #if filesToLoad ends before the current batch finishes
				break
			print('Progress: Downloading file ' + str(N*i+j) + '/' + str(len(filesToLoad)))
			sys.stdout.flush()
			tempFile = drive.CreateFile({'id': filesToLoad[N*i+j]})
			tempFile.GetContentFile('tempFile' + str(j) + '.pdf') #Download file
			merger.append('tempFile' + str(j) + '.pdf')
		if (i < N-1): #If you still have more batches to go, save temporary output file
			merger.write('tempOutputFile' + str(i) + '.pdf')
			merger.close()
			merger = PdfFileMerger()
			merger.append('tempOutputFile' + str(i) + '.pdf')
		else: #Done with all the batches, so save to the final output file
			print('Now merging files. Please wait for a bit...')
			merger.write(outputFileName)
			merger.close()
		
	#Delete temporary files
	for j in range(maxFilesPerBatch):
		os.remove('tempFile' + str(j) + '.pdf')
	for i in range(N-1):
		os.remove('tempOutputFile' + str(i) + '.pdf')
	return True

def parseAndMergePdfs():
	if len(sys.argv) == 4:
		inputFile, targetColumn, outputFile = sys.argv[1], sys.argv[2], sys.argv[3]
		filesPerBatch = 10
		upload = False
	elif len(sys.argv) == 5:
		inputFile, targetColumn, outputFile, filesPerBatch = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]
		upload = False
	elif len(sys.argv) == 6:
		inputFile, targetColumn, outputFile, filesPerBatch, upload = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]
		if upload == 'True':
			upload = True
		else:
			upload = False
	else:
		inputFile = 'pdfList.csv'
		targetColumn = 2
		outputFile = 'output.pdf'
		filesPerBatch = 10
		upload = False
	
	gauth = GoogleAuth()
	gauth.LocalWebserverAuth() # Creates local webserver and auto handles authentication

	drive = GoogleDrive(gauth)
	filesToLoad = parseFileNames(inputFile, int(targetColumn))	
	print('Files to load: ' + str(filesToLoad))
	filesPerBatch = min(int(filesPerBatch), len(filesToLoad))
	mergeFiles(filesToLoad, outputFile, drive, maxFilesPerBatch=filesPerBatch)
	
	if upload == True:
		fileToUpload = drive.CreateFile()
		# Read file and set it as a content of this instance.
		fileToUpload.SetContentFile(outputFile)
		fileToUpload.Upload() # Upload the file.
		fileToUpload = None
		print('File uploaded to drive successfully. Deleting local version of output file...')
		os.remove(outputFile)
		print('Local output file deleted')
		

if __name__ == "__main__":
    parseAndMergePdfs()
