# CSVParser
===========

This is a simple script to parse through and perform various options on the data in CSV files.

Requirements
-----------
* [PyDrive](http://pythonhosted.org/PyDrive/index.html)
* [PyPDF2](https://pythonhosted.org/PyPDF2/index.html)

Steps to setup
-----------
(these steps are for Windows; look at the specific installation websites for details on how to set up software for Macs or other systems)

1. Download PyDrive. You can use `pip install PyDrive` or visit the website to download the latest version through Github
2. Download PyPDF2. You can use `pip install PyPdf2` or visit the website to download the latest version through Github
3. Setup your Google Drive account so it can be accessed remotely through the program (these steps are copy-pasted from the PyDrive page)
	1. Go to [APIs Console](https://console.developers.google.com/iam-admin/projects) and create a project (the project or product names doesn't matter for the rest of this project, so pick anything for this field).
	2. Search for ‘Google Drive API’, select the entry, and click ‘Enable’. Since the project takes a few minutes to appear on the main page, you may want to check your notifications (the bell to the top right of the center panel) for quick access. Once there, select the project you just created, select the 'API's' box, and select 'Google Drive'.
	3. Select ‘Credentials’ from the left menu, click ‘Create Credentials’, select ‘OAuth client ID’.
	4. Now, the product name and consent screen need to be set (again, these names don't really matter for this project). After that, click ‘Configure consent screen’ and follow the instructions. Once finished:
		1. Select ‘Application type’ to be Web application.
		2. Enter an appropriate name.
		3. Input `http://localhost:8080` for ‘Authorized JavaScript origins’.
		4. Input `http://localhost:8080/` for ‘Authorized redirect URIs’.
		5. Click ‘Save’.
	5. Click ‘Download JSON’ on the right side of Client ID to download client_secret_<really long ID>.json.
	6. The downloaded file has all authentication information of your application. Rename the file to “client_secrets.json” and place it in your working directory.
4. Download this repository with `git clone https://ahad_rauf@bitbucket.org/ahad_rauf/csvparser.git` or another method of your choice
5. Move the "client_secrets.json" file into the folder
6. Add the .csv file your want to parse (currently, this project only supports comma-separated CSV files. If you have a tab separated one, change the comma on line 37 of the code to a space (the line is `reader = csv.reader(csvfile, delimiter=',', quotechar='|')`)
7. If necessary, change the regex formula on line 39 to one that suits your needs to extract the file ID's (the line is `matchObj = re.match('.*id=(.*)', row[targetColumn])`)
8. Finally, time to run the function! Run it with one of these three commands:
	1. `python2 drive-parser.py <inputFile> <targetColumn> <outputFile>`
	2. `python2 drive-parser.py <inputFile> <targetColumn> <outputFile> <maxFilesPerBatch>`
	3. `python2 drive-parser.py <inputFile> <targetColumn> <outputFile> <maxFilesPerBatch> <upload>`
	Note that you can also use `py -2 drive-parser.py` instead if you want.
	<inputFile> is the .csv file you want to read from (ex. input.csv)
	<targetColumn> is the column that contains the links to the PDF's you want to merge (ex. 2)
	<outputFile> is the name of the final PDF that contains all the merged PDF's (ex. output.pdf)
	<maxFilesPerBatch> is an internal parameter that refers to the maximum number of files you have downloaded at once to save memory (ex. 10)
	<upload> is a True/False variable for whether you want to upload your file to your Drive after merging (by default False)
9. Run the function and watch the magic run. Note that you'll have to log into your account to authenticate your access (the page for this will appear automatically).